# 3. MODULE 3 : IMPRESSION 3D

Pour ce troisième module, nous avons été formé à utiliser les imprimantes 3D. Notre salle est équipée des modèles **Original Prusa i3 MK3S**, des imprimantes 3D fabriquées elles même par des imprimantes 3D, ce qui est particulièrement avantageux lorsqu'une pièce se casse puisqu'on peut la reproduire à l'aide d'une autre imprimante. Ce type d'imprimante est conçu dans un espace qu'on appelle **ferme d'impression 3D**, c'est assez impressionnant à voir. 

Je vais donc vous expliquer ce que j'ai appris à faire durant ce module, mais aussi, vous montrer mes **échecs** (parce qu'il y en a) et c'est important de voir l'évolution de mon travail pour comprendre comment je suis arrivée au bout de mon projet.


## L'IMPRIMANTE

Je vais vous parler machine avant de vous parler du reste : Pour commencer il a fallu installer un logiciel pour que la machine sache comment imprimer notre objet en 3D. Pour ça on s'est rendu sur le site de [Prusa](https://www.prusa3d.com/drivers/) dont le concepteur à donner le nom à ses machines. Il a été nécessaire de télécharger un software propre aux machines se trouvant dans le FabLab : celui pour **PRUSA I3MK3S**. Je suis allée, pour trouver celui-ci, dans Software > Drivers, firmware and manuals > ORIGINAL PRUSA I3 MK3S > DRIVERS & APPS 2.2.9.1 (pour windows c'est en tout cas celui dont on a besoin).

### Aspect pratique 

D'abord on travaille avec des fichiers au format STL sur **Fusion 360**, puis on importe ce fichiers dans le logiciel téléchargé juste avant, nommé **PrusaSlicer**. Donc une fois le document Fusion 360 terminé, on l'exporte en STL et on l'enregistre quelque part sur notre bureau. On lance ensuite le logiciel PrusaSlicer et on importe le document STL. Si tout ce passe bien, et il n'y a pas de raison que ça se passe mal, notre objet apparait alors sur le plateau 3D de l'imprimante, dans le logiciel PrusaSlicer. 

A partir de là, on peut faire ce que l'on veut avec notre objet pour que l'impression se passe au mieu : D'abord on choisit la face de contact entre l'objet et le plateau, pour ça on peut facilement retourné l'objet dans tous les sens. Le but est évidemment de limiter au maximum les renforts et les porte-à-faux lors de l'impression. Pour cela la surface de contact doit être maximale et logique par rapport à la formede l'objet. Evidemment dans mon cas par exemple, il y aura besoin de renforts pour soutenir mes porte-à-faux au niveau des système de rotation, mais la configuration idéale reste la forme repliée de mon objet pour ce genre d'impression. Si les arcs avaient été dépliés cela n'aurait pas été possible, où en tout cas complexe pour conserver l'aspect modulable de la pièce. 

Ensuite arrive l'aspect technique : Le fil que l'on utilise pour ces machines est appelé filament. Dans notre cas on dispose du prusament PLA. Il existe des fils de différentes épaisseurs. Dans notre FabLab, le filament utilisé permet une hauteur de couche de 0.2mm. Plus l'objet est gros, plus c'est long, et le temps augmente très vite, donc le but n'est pas de faire un gros objet mais plutôt avoir un premier jet de ce qu'il est en ayant compris le fonctionnement des machines. 

#### Paramètrage et chiffres

Il y a des petites choses à savoir pour faire un bon paramètrage de **PrusaSlicer**, par exemple c'est important de savoir que la buse de l'imprimante 3D peut aller jusqu'à 0.4 mm d'épaisseur de trait. Cela l'ai d'autant plus dans mon projet puisque les paroies sont très fines et fragiles à petite échelle. Il y a aura donc une échelle minimale à ne pas dépasser. 

Pour bien paramètrer l'impression 3D, il faut déjà passer en mode expert sur PrusaSlicer, cela permet de débloquer des paramètrages supplémentaires et ainsi être plus précis dans sa programmation. Devant chaque paramètrage apparait un hexagone coloré. Si il est vert, ça veut dire que le réglage est simple, si il est orange, il s'agit d'un réglage avancé, et si il est rouge, c'est un réglage d'expert. Il faut également savoir que si le paramètre devient orange et n'est plus noir comme les autres, c'est qu'il à été modifié. Maintenant que vous savez cela, je vais présenter étape par étape ce que l'on à appris du paramètrage du logiciel grâce à des captures d'écran du logiciel : 

- 1 : Couche et périmètre : Pour la hauteur de couche, il faut toujours mettre 0.2mm et ne pas toucher à celle de la première couche. Pour les parois vérticales, le périmètre doit être à 3 par défaut pour le minimum afin d'avoir une certaine solidité de l'objet. Après libre à nous de modifier le type de remplissage intérieur, cela dépendra de la resistance de l'objet attendu, mais ce sera pour l'explication suivante ça. Pour le reste, on ne touche à rien sauf si cela est utile dans le cas d'un objet qui en nécessiterait. Sinon on peu laissé par défaut. 

![couche et périmètre](../images/capt1.JPG)

-2 : Remplissage : La densité de remplissage doit idéalement toujours être comprise entre 10 et 15% si la résistance mécanique de l'objet est faible. On peut monter jusqu'à 35% si vraiment cela est nécessaire mais pas au dessus, ce serait du gaspillage de matière. D'ailleurs les formes de remplissage les plus efficaces sont les formes gyroïdes et en nid d'abeille. 

![remplissage](../images/capt2.JPG)

- 3 : jupe et bordure : Il y a par défaut une jupe de programmé sur l'objet pour que celui-ci soit bien stable sur le plateau, mais si la pièce que l'on imprime en 3D est grande (haute surtout), on va demander à créer un contour supplémentaire pour stabiliser l'objet lors de son impression et éviter qu'il ne tombe ou s'imprime mal. La bordure, si on la rajoute, permet donc d'avoir une plus grande emprise au sol, il s'agit d'une légère couche supplémentaire sur le plateau, autour de notre objet, qui maintient d'avange la pièce. 

![jupe et bordure](../images/capt3.JPG)

- 4 : supports : on peut générer des supports supplémentaires pour maintenant une partie de l'objet qui serait en porte-à-faut ou aurait besoin de support pour ne pas tomber. Il suffit de cocher la case. De préférence, il vaut mieux les générer uniquement sur le plateau de l'imprimante : cela permet une économie de matière et de temps surtout. Plus on utilise de la matière, plus ce sera long et couteux. 

![supports](../images/capt4.jpg)

- 5 : pour les autres réglages, il vaut mieux laisser par défaut et jouer éventuellement avec si on possède un cas spécial, sion pas besoin d'y toucher. Au niveau du filament, il faudra bien paramètrer la machine en fonction du filament qu'on insère dedans. Si on a un doute sur la quantité de fil qu'il reste et qu'on pense que la machine n'ira pas au bout de la pièce, on peut soit mettre en pause et changer la bobine vide en cours d'impression, soit par sécurité la changer avant de commencer, mais il ne faut surtout pas que la bobine soit vide en cours d'impression et qu'elle chauffe à vide. Ca pourrait l'abimer. 

Il faut aussi prévoir la taille de l'objet final avant de lancer l'impression ! Le plateau n'est pas très grand, il faudra donc bien gérer les échelles de son projet. La taille du plateau est à peu près de 20 x 20 cm et 22cm de hauteur. 

## LANCER UNE IMPRESSION

Nous y voilà, après avoir tout bien paramétré, c'est l'heure de l'impression. On **lance le découpage** sur l'interface de base. Petite astuce, si on veut mettre à jour le résultat final de ce à quoi va ressembler l'impression après avoir changé des paramètres, on appuit sur sur la touche TAB avec les deux flèches inversées, et cela met à jour la visualisation 3D finale. C'est très utile surtout quand on rajoute des renfort ou une bordure pour visualiser cela. Par exemple, dans mon cas ci-dessous, j'ai ajouté une jupe et des supports pour le système de rotation qui était en porte-à-faux, donc la pièce qui sortira ressemblera à ça : 

![final pursa](../images/capt5.JPG)

Une petite précision concernant les plateaux des imprimantes : il est essentiel de les nettoyer à l'acétone avant utilisation pour garantir un maximum d'adhérence à la pièce dessus. Une fois que tout semble bon, on **exporte le G-code** de PursaSlicer, ce G-code devra être copié sur une carte SD d'une des imprimantes, dans un dossier comportant notre nom.prenom. Le dossier étant sur la carte SD, la machine va la détecter et nous proposer d'ouvrir le fichier. Elle va commencer l'impression une fois que la machine sera montée à la bonne température (250°). Il est essentiel de rester à proximité au moins pour voir si les **trois premières couches** se passent bien. Dans le cas contraire il faudra stopper l'impression. Il faut aussi faire très attention à la vitesse d'impression et idéalement ne jamais dépasser les 100%, plus la machine est lente, mieux c'est, surtout au début. 


## RESULTATS ..... ECHECS

![final pursa](../images/echec.jpg)

Deux tentatives, deux échecs ... Je m'y attendais vu la taille de l'objet et le degré de précision attendu mais bon au moins j'aurai essayé et je sais que ca ne vient pas d'un mauvais paramètrage. Le premier cas, il s'agit d'un échec du à de l'inattention, je n'ai pas fait attention à la vitesse et quelqu'un avait du l'augmenter avant moi, j'imagine que les arcs se sont décalés à cause de cela. C'était une petite pièce qui demandait de la précision, de la lenteur et la vitesse était sur 175%. J'ai donc du stopper la machine quand je suis retournée dans la salle pour voir l'avancement, afin d'éviter de gaspiller plus de matière. Dans le deuxième cas, quand je l'ai relancé, tout semblait bien fonctionner, je suis passé plusieurs fois suivre l'avancement, puis arrivé à environ 0,7mm de hauteur, tout s'est à nouveau décalé. 

Les salles du FabLab devaient fermer donc je n'ai pas eu le temps de relancer une autre impression et avoir des conseils pour que celà fonctionne mais je pense que le problème venait principalement de la taille de mon objet et du degré de précision qui lui était nécessaire. La buse ne peut pas aller en dessous de 0.4mm, avec une rêgle de trois, j'ai calculé l'échelle minimale que pouvait avoir mon objet (1/7,5ième). Je l'ai réduit un peu moins que le minimum pour que cela ne soit pas trop petit (1/5ième), mais malgré cela, je pense que c'est tout de même très limite. La prochaine fois, je retenterai à une échelle supérieure, j'ai d'ailleurs recommencé ma modélisation 3D (celle expliquée lors du module 2) pour m'assurer que chaque corps seraient sans défaut maintenat que je maîtrise mieux le logiciel. J'attend désormais de pouvoir passer au FabLab pour relancer tout ça. 

En bonus, je vous offre un petit extrait de mon impression quand elle était encore bien partie, pour vous montrer le coté hypnotique de la chose ! 

 ![vidéo](../images/video-1603028273_eOOeE7C0.mp4)



### RECOMMENCEMENT

Me revoilà, pour une troisième tentative. Après avoir remodéliser mon objet sur **Fusion 360**, revu les paramètres de **PrusaSlicer** avec une personne compétente, je retente l'expérience. Cette fois-ci, pour m'assurer que ce ne soit pas un echec, j'ai augementé l'échelle de mon objet : depuis le logiciel PrusaSlicer je fait en sorte que l'échelle soit de 30%, c'est-à-dire que que mon objet fera 1/3 de sa taille réelle. 

L'impression est, à l'heure où je vous écris, toujours en cours mais elle semble prometteuse. Forcément elle est longue comme mon objet est plus grand que la dernière fois, mais pour le moment aucun loupé et elle à atteint facilement 2.5 cm de hauteur ! C'est bon signe. 

![impression en cours](../images/bleu.jpg)

Je vais tout de même remettre des captures d'écran de mes paramètres pour montrer ce que j'ai modifié avec l'aide de Gwen et Elene, les deux intervenantes du FabLab, pour m'assurer d'avoir une bonne pièce : 

![](../images/F1.jpg)

- L'image 1 : montre ce que l'on obtient lorsqu'on importe le fichier STL sur **PrusaSlicer**. La colorimètrie s'adapte à la fonctionnalité de chaque partie de l'impression représentée. 
- L'image 2 : montre la programmation de l'option **"couche et périmètre"**. Rien ne change sauf pour "coques horizontales", j'avais oublié de mettre 3 pour les deux couches solides. En soit ce n'est pas ça qui a fait louper mon impression mais c'est important. Pour le reste rien ne change. 

![](../images/F2.jpg)

- L'image 3 : montre la programmation de **"remplissage"**. J'ai modifié les motifs de remplissages, pour que l'impression prenne moins de temps et de matière, un motif triangulaire suffit largement pour la résistance de mon objet et demandera beaucoup moins de matière/temps. Pour le reste j'ai précisé ne faire du remplissage que là où ça semble nécessaire pour rester logique dans mon économie de moyens.
- L'image 4 : montre la programmation de **"jupe et bordure"**. J'ai rajouté une bordure de 3 à mon objet puisque celui-ci est particulièrement haut et fin, ce qui permet de mieux le stabiliser sur la plaque comme expliqué plus haut. Sinon rien ne change.

![](../images/F3.jpg)

- L'image 5 : montre la programmation de **"supports"**. J'ai généré des supports supplémentairespour soutenirs mes objets en porte-à-faux, comme mentionné plus haut, mais rien ne change des paramètres que j'avais encodé.
- L'image 6 : montre la programmation de **"filament"**. Inutile mais sait-on jamais, j'ai mis la bonne couleur pour mon filament c'est-à-dire bleu. Sinon je ne modifie rien d'autre. 

![](../images/final7.JPG)

Si on retourne sur l'image du plateau, on voit désormais tous nos paramètres encodés et on peut faire descendre la ligne du temps qui se trouve à droite (flèche orange) pour s'assurer du détail de fabrication de l'intérieur de notre objet (voir image ci-dessus). J'ai ensuite reçu la validation des intervenantes pour relancer mon impression après un dernier chek-up. Je publierai le résultat final le plus rapidement possible dès que j'aurais celui-ci en main propre.

# UPDATE !!!!!! 

![](../images/resultat1.jpg)

Mon impression c'est terminée avec succès, j'ai eu un peu peur de décoller les renforts parce que l'axe de rotation est très fin, mais finalement, tout c'est bien passé et le mécanisme fonctionne vraiment ! L'axe de rotation est un peu long, ce qui fait que les arcs ne savent pas tenir seul mais je n'avais pas le choix, je devais exagérer les mesures pour réussir la contrainte de tout imprimer en une fois ! Par contre les jupes qui maintenaient l'objet sur le plateau ont été particulièrement difficile à retirer, j'ai fini le travail à la lime à ongle. 

![](../images/resultat2.jpg)

Je me suis amusée à disposer une ampoule à l'interieur, mais je ne m'attendais à un résultat aussi efficace ! Malgré l'épaisseur des couches de filament PLA, la lumière se diffuse parfaitement ! Pour maintenant les arcs en position, j'ai simplement resseré ceux-ci avec un petit bout de carton entre la rondelle et eux, pour augmenter les frottements et ainsi permettre aux arcs de conserver leurs positions en étant moins libres dans leurs mouvements autour de l'axe. 

![](../images/resultat3.jpg)

Je suis vraiment très satisfaite du résultat ! Me voilà arrivée au bout du module, après plusieurs tentatives, mais ça vallait le coup.






