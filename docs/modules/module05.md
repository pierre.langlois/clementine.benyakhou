# MODULE 5 : SHAPER

Aujourd'hui, petite formation sur la **Shaper**, une machine semblable à une mini CNC portative, qui permet de faire des fraisages de précision sur des matériaux non limités par leurs tailles. Tu peux trouver la machine, son guide et ses outils sur le [site officiel](https://www.shapertools.com/fr-fr/) et avoir des renseignements sur son fonctionnement sur le [site du FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Shaper.md). Si tu as déja utilisé la machine de découpe laser, la Shaper sera d'une grande facilité ! Il faut juste prendre conscience de certaines normes de sécurité et du protocole de mise en marche. Pour ça pas de soucis, je vais t'expliquer l'essentiel à savoir. 
Puis si tu veux aller plus loin que mes explications ou mieux comprendre un détail, je te conseille vivement d'aller voir le guide d'utilisation qui existe en pdf (en anglais ou en français) sur [l'adresse suivante](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf).

![Shaper](../images/shapshap.jpg)

## LES BASICS A CONNAITRE

- Profondeur de découpe maximale : 43 mm

- Diamètre du collet : 8 mm ou 1/8"

- Format de fichier importé : SVG

- Profondeur : dépend du diamètre de la fraise

- Abre pour la fraise : 6 mm

## DRESS CODE 

Du point de vue "dress code", la rêgle est simple : rien qui risque d'être pris dans la machine ! C'est à dire : des cheveux attachés si ils sont longs, pas de bijoux, et surtout, pas de vêtement amples ! Le dress code ne s'arrête pas là, le manuel d'utilisation de la machine recommande également le port de lunette/masque, et d'un casque anti-bruit. Mais bon il faut savoir que la machine n'est pas très bruyante, alors sauf en cas d'utilisation prolongée, inutile d'aller jusqu'à porter un casque, et pour ce qui est du masque et des lunettes, un aspirateur branché à la machine empèche toute projection de sciure de bois, donc cet équipement peut être supprimé en cas de présence d'un bon aspirateur relié à la machine. 

## INSTALLATION DU PERIMETRE DE TRAVAIL

### Fixation
Pour une installation du périmètre de travail, il faut avant toute chose s'assurer d'avoir une surface suffisamment dégagée et stable avant de commencer quoi que ce soit. Si tel est le cas, il faut alors se munir de son matériaux et fixer celui-ci parfaitement à la table de travail afin que les morceaux découpés ne "sautent" pas. Pour le fixer ce n'est pas compliqué : si la surface de travail est stable et lisse (pas en OSB par exemple), alors il suffit de mettre du double face solide sur toutes les parties nécessaires de la planche. Il ne faut pas hésiter à en mettre bien partout, mais sans abuser non plus. L'idéal est de réfléchir à sa forme de découpe et anticiper celle-ci de manière à savoir ou placer le scotch judicieusement. Si ce n'est pas le cas, ou dans le doute, afin de garantir une bonne tenue de notre matériaux, on peut aussi visser la planche au plan de travail. Celle-ci ne bougera pas. Ou alors utiliser des serre-joints au quatre coins + du scotch sur les découpes. Dans l'exemple ci-dessus, dans le cas d'une petite planche en bois légère pour une découpe simple, le double-face à suffit. Il ne faut pas non plus travailler en bord de table au risque de faire tomber la machine et le matériaux. On doit être stable et notre espace de travail dégagé pour de bon mouvement.

![position](../images/POSITION.jpg)

### Délimitation
La machine utilise des repères spatiaux qui ressemblent à des dominos. Ces repères se trouvent sur du scotch mis à notre disposition, il suffit de le coller sur notre planche et de faire un scan avec la caméra de la machine afin qu'elle prenne conscience de l'espace de travail dont nous disposons, mais aussi des dimensions, et des repères spatiaux pour les découpes. Evidemment, ces scotch doivent être placés à des endroits stratégique dans le champs de vision de la machine. Par soucis d'économie, il faut en utiliser le moins possible et donc les découpés avec un minimum de 4 afin d'en utiliser le moins possible sur la plus grande surface possible. Inutile d'en mettre partout, il en faut juste suffisamment pour que la machine sache se répérer, si ce n'est pas le cas, elle nous l'indique avec le voyant "domino" en haut à droit de l'écran qui passe de gris foncé (bon), à gris clair (manque de domino) à rouge (découpe impossible). 

## PRECAUTION D'USAGE

Avant toute chose, voila deux petites images pour vous expliquer comment est composée la machine : 

![machine](../images/shhh.jpg)

### Gestion de la fraise
Il y a deux boutons situés de chaque coté de la machine, à l'endroit où on la prend en main via les poignées. Un vert à droite pour lancer le fraisage et donc faire descendre la fraise, et un orange à gauche pour la faire remonter. Attention, en cours d'utilisation, la fraise sera forcement baissée, mais lors de la fin de la découpe, elle ne se relève pas seule. Pour éviter l'impacte avec notre matériau (la fraise est très fragile), il faudra penser à relever la fraiseuse entre deux découpes différentes ou lors de la fin d'une découpe. Pour cela, il faut donc appuyer sur le bouton orange (poignée de gauche). 

###Changer la fraise
Pour changer la fraise, il faut utiliser une clé pour desserer le collet puis le resser. Mais ce n'est pas tout, il y a des choses à faire dans un certain ordre : tout d'abord, il faut couper la machine, débrancher la fraiseuse du reste de la machine puis dévisser avec la papillon cette dernière. Lorsque la fraiseuse est indépendante de la machine, la soulever par le haut sans cocher la fraise. Lorsqu'elle est sortie , la posé à l'envers, et dévisser en faisant trois tours avec une clef pour desserer la fraise. Lorsque celle-ci est suffisamment débloquée, la changer sans pousser sa remplacante jusqu'au fond de la fraiseuse (c'est inutile il faut juste revisser pour garantir son blocage). On ressère ensuite à l'aide de la clé. Puis de la même manière que pour sortir la fraiseuse de la machine, on la remet dedans, on pense à bien **revisser la fraiseuse avec la vis papillon** puis on rebranche le cable pour la connecter à la machine. Si on oublie de resserer la fraiseuse, la machine n'aura plus de résistance mécanique et risque de se casser.  Ci-dessous, l'image qui montre comment enlever la fraise. 

![changer fraise](../images/yiuty.JPG)

### Vitesse de fraisage
Pour des découpes propres, il faut adopter une bonne vitesse de fraisage. Si la vitesse est trop basse, il risque d'y avoir des acouts et la ligne de découpe ne sera pas lisse et régulière. Au contraire, si la vitesse est trop élevée, il y a un risque de brûler la matière, voir de faire surchauffer la fraise et la briser. Afin de trouver la bonne vitesse de réglage, il est important de faire des essais avant dans un coin du matériau ou de se renseigner sur celui-ci. La vitesse peut varier entre 10 mille et 25 mille tours par minute. 

### Précision du dessin 
Pour un dessin sans risque de brulure ou d'acouts, il est préférable de faire plusieurs passes, d'autant plus si le matériaux est très épais. C'est comme pour la découpe Laser. Le nombre de passe dépendra évidemment du diamètre de la fraise et de son efficacité à faire un dessin précis. Si la pièce découpée sert à un emboitement (puzzle ou autre), on peut aussi utiliser l'outil **offset** qui rogne les angles de la pièce ou du percement, permettant d'avoir plus de jeux lors de l'emboitement des deux pièce et donc garantir le bon fonctionnement. 

## MISE EN MARCHE 

L'image ci-dessous montre les différetes parties de la machine, non pas en schéma, mais en application réelle afin de mieux comprendre comment elle se compose.

![schema nom](../images/fraise.jpg)

- 1 : Vérifier que les normes de sécurité sont respectées (Dress code et installation de l'espace de travail).

- 2 : Etablir son périmètre de travail.

- 3 : Brancher l'aspirateur.

- 4 : Brancher la machine (elle s'allume seule sans bouton lors du branchement).

- 5 : établir un scan du périmètre de travail pour que la machine se repère sur le matériau découpé. Pour cela il faut la faire bouger sur notre espace à découper et s'assurer que la position des dominos soit bonne. On lance la séléction du scan avec le bouton vert. Puis on fait terminé le scan avec le bouton orange. Notre espace de travail est désormais enregistré. 

- 6 : Importer notre fichier SVG importé au préalable sr la clé USB. Insérer la clé dans la machine, puis cliquer sur "dessiner" pour visualiser la projection du dessin sur notre matériaux sur l'écran. Quand l'emplacement du dessin est bon faire "placer". 

- 7 : Lancer "fraiser" pour entammer les réglages (type de découpes : intérieures, extérieures, au milieu du dessin / diamètre de la fraise (attention le diamètre limite la profondeur du fraisage) / la profondeur du fraisage). 

![lignes](../images/ligne.jpg)

- 8 : Faire "Z TOUCH" pour tater la profondeur du matériaux (surface avant de commencer). 

- 9 : Allumer la fraiseuse en cliquant sur le bouton vert et lancé le fraisage, suivre les mouvements des flèches indiquées. 


## DETAILS ET INTERFACE

![détails](../images/détails_machine.jpg)

Quelques détails sur l'image ci-dessus pour mieux comprendre comment se compose la machine : 

- 1 : Papillon pour retirer la fraiseuse et différente fraises de plusieurs dimensions.

- 2 : Projection du dessin SVG sur le matériaux scanné au préalable. Le tour du S en gris montre que nous avons selectionné une découpe extérieure au traits du dessin. 

- 3 : Jauge des dominos (en haut à droite) dans le cas présent, colorée en rouge, car les dominos ne sont pas assez présents. Le dessin en bleu correspond au tracé dans le bois que la fraiseuse supprimera de notre planche. 

## CONCLUSION 

Voilà, les explications sont plutôt courtes en comparaison aux autres modules mais l'utilisation est super simple, et si tu as compris la découpe laser ce sera un jeux d'enfant pour toi ! Donc suis bien les recommandations pour la mise en place et les réglages, et ensuite laisse toi guider pas la machine ! Tout devrait fonctionner à la perfection, et en cas de doute, réfère toi au PDF mis à ta disposition au début de ce module ! 
