# 1. MODULE 1 : INSTALLATION GIT

Cette semaine, nous avons commencé le premier module, celui-ci était divisé en deux parties. Dans un premier temps, nous avons eu une initiation à **Fusion 360**, un logiciel de conception et rendu 3D. Nous avons ainsi appris les bases de ce logiciel. Puis, dans un second temps, nous avons abordé les quelques notions de base de **codage** nécessaires pour l'utilisation de GitLab et faciliter la publication de nos résumés. Le but de cette plateforme est de partager et documenter facilement et rapidement nos connaissances.

L'objectif n'est pas d'avoir une belle page bien léchée, mais plutôt de conserver une trace écrite de mon apprentissage. Je vais ainsi vous détailler, comment j'ai appris, lors de ce module 1, à utiliser ce site web, et par quels procédés d'apprentissage j'ai du passer pour que vous aussi vous puissiez le maîtriser à votre tour. 

## CONFIGURATION GITLAB

### Rediger du texte

Pour rédiger cette page documentaire, j'aurais pu me contenter d'écrire directement via la plateforme de **GitLab**, puisque celle-ci modifie automatiquement le dossier écrit grâce à **Mkdocs** (un générateur de site écrit en Python). C'est d'ailleur ce que je me suis limitée à faire pour écrire ma partie "Présentation" et prendre la main avec le site. Pour cela il suffit de personnaliser le fichier mkdocs.ymt avec nos informations, remplacer les données de bases inscrites et le tour est joué.

Mais cette solution ne permet pas d'aborder la manière la plus rapide et efficace que nous offre de faire le **codage**. Alors pour personnaliser mon projet final et écrire le module 1, j'ai du télécharger **Git**. Il s'agit d'un logiciel de gestion souvent utilisé pour gérer de la documentation. Il permet aussi de travailler à plusieurs sur un même projet. Voir la photo ci-dessous :

![git](../images/git.JPG)

Puis il a été nécessaire d'installer **Shell Bash** pour faire fonctionner tout ça. La fusion de Bash et Git est appelée **Git BASH**. Shell Bash se trouve par défaut sur les ordinateurs sous Windows 10, mais c'est une fonction cachée qu'il faut activer. Me voilà chanceuse à ce niveau là, c'est mon cas. Pour cela, le chemin est un peu complexe mais il vaut d'être fait par la suite. Je vais expliquer rapidement les étapes mais le tutoriel vidéo et écrit sur [Korben](https://korben.info/installer-shell-bash-linux-windows-10.html) est très bien détaillé et efficace pour réussir cela si mes explications ne sont pas suffisantes: 

- 1. Pour les ordinateurs comme le mien, fonctionnant sous Windows 10 : aller dans "Paramètres" > "Mise à jour et sécurité" et passer en mode "Pour les développeurs". La première étape est terminée.

- 2. Dans l'application "Fonctionnalité", dans "Activer ou désactiver des fonctionnalités Windows" il va falloir cocher la case "Sous-système Windows pour Linux" puis attendre que l'ordinateur se redémarre pour être fonctionnel. 

- 3. Une fois l'ordinateur rallumé, ouvrez le terminal de commande et tapper les commandes suivantes (voir photo) pour s'assurer que tout à fonctionné.

![installation GIT](../images/git_version.JPG)

Voilà ! Vous venez de créer **Git BASH**, il sera nécessaire de le configurer en s'identifiant directement via le terminal avec son nom et son e-mail (voir photo ci-dessous). Attention toutefois, ne faites pas la même erreur que moi, pensez à bien prendre le même nom et e-mail que celui utilisé sur GitLab sinon cela ne fonctionnera pas. Si vous avez fait la même erreur, pas de panique, il suffit de recommencer et le logiciel prend en compte les dernières informations.

![](../images/config_nom.JPG)

Pour vulgariser cela et mieux comprendre, pour les novices comme moi, **Git BASH**, c'est le système de commande rapide qui permettra de prendre les commandes de l'ordinateur et de différents logiciels, on parle aussi de terminal. Il permettra de gagner beaucoup de temps et diminuer les actions considérablement pour un résultat plus efficace, à condition de comprendre un minimum le codage. 

- 4. Pour connecter mon ordinateur à **GitLab**, j'ai du ajouter des informations de sécurité pour m'authentifier, en plus de configurer mes identifiants. Jusqu'ici c'est simple, je dispose de plusieurs possibilités : m'authentifier via HTTPS ou via une clé SSH. J'ai décidé de prendre la méthode de la clé SSH car une fois authentifié, GitLab ne me demandera plus d'informations d'identification, ce qui simplifie les chose. C'est ensuite que ça se complique : il va falloir créer une nouvelle clé SSH. 

- 5. Pour créer une clé SSH, il faut déjà se décider : créer une clé ED25519 ou une clé RSA ?? Personnellement j'ai choisi la clé ED25519 car elle est plus sure et performante que la RSA. 

![clé SSH](../images/clé_ssh.JPG)

Avec ceci, on peut alors obtenir une clé, semblable à celle ci-dessous : 

![clé finale](../images/clé_finale.JPG)

Lorsque cette clé est donnée, elle est aussi enregistrée dans votre ordinateur, pour ma part, elle se trouve dans mon disque C. dans un dossier qui est intitulé .ssh. Dans ce dossier on trouve une clé privée et une clé publique, il faudra de préférence travailler avec la publique (notée .pub comme le montre la photo ci dessous).

![clé sur bureau](../images/stock_clé.JPG)

- 6. Pour configurer GitLab et ajouter la clef SSH, rien de plus simple, il suffit d'aller sur GitLab, ouvrir la clé fraichement créée (avec un logiciel de texte comme **Atom**, idéalement avec le package "markdown-preview-enhanced"), copier la clé fournie et aller dans les "paramètres" de l'espace personnel GitLab pour la copier en complétant la case SSH. La clef SSH est ajouté à GitLab !

![](../images/ajout_clef_ssh.JPG)

Pour vérifier l'identification, il suffit de tapper le code suivant, entrer, et voilà, tout est fait, bienvenue sur GitLab !! 

![identification](../images/welcome_to_gitlab__.JPG)

-7. L'étape finale consiste à créer un référentiel distant pour **GitLab**. C'est-à-dire qu'un projet spécifique dans GitLab correspond à un dossier spécifique sur mon ordinateur local. Pour cela, il faut se rendre sur son compte **GitLab** et cliquer sur "clonage". Séléctionner la clé SSH et copier là dans un logiciel de texte. 

![clonage](../images/clone_ssh.JPG)

Il faut ensuite choisir un dossier sur le bureau où l'on veut créer le référentiel, ouvrir un terminal **Git BASH** dans ce dossier (clic droit dans le dossier) et ouvrir la clé SSH clonée. Le fichier se crée alors seul à l'emplacement désiré, l'ordinateur fusionnera avec **GitLab** comme en témoigne mon terminal : 

![terminal référentiel](../images/cloner_git_sur_bureau.JPG)


## ENRICHIR SES DOSSIERS

### Ajouter et modifier du texte

Avec l'étape précédante, c'est-à-dire la création du référentiel, on peut désormais modifier directement tout sur le bureau sans avoir à aller sur **GitLab**. On peut y ajouter des photos, modifier le texte avec un editeur de texte tel que **Atom**. C'est super rapide !

### Modifier le texte dans Atom 

Je vous explique rapidement comment faire, mais l'idéal c'est de suivre, ce [tuto](https://courses.cs.washington.edu/courses/cse154/19su/resources/assets/atomgit/windows/) qui récapitule parfaitement bien les étapes. Une fois le référentiel local existant créé, on peut l'ouvrir dans **Atom**, ce qui permet d'utiliser Git directement dans Atom. Si Atom affiche un petit logo de livre en haut à gauche, c'est qu'il reconnait bien le dépot de projet de GitLab et la connexion existante entre l'ordinateur et la plateforme. En cliquant dessus, nos fichiers Gitlab s'affichent en copie locale (formatée avec les spécificité de codage d'Atom). Là on peut se permettre de modifier le texte et faire tous les ajouts que l'on désire. 

Maintenant, la difficulté c'est de pouvoir faire valider cette version de la modification pour que le transfert s'oppère sur la plateforme aussi afin de synchroniser les documents en ligne. Pour cela, il faudra penser à "valider" et "pousser" les versions modifiées d'Atom avec **GitBash**. Deux commandes sont alors essentielles à retenir : 

- Git push
- Git pull 

Avec Atome il faut ouvrir le référentiel Git dans le référentiel du projet. Avant cela, il faut s'assurer que les modifications du document sont bonnes. Pour cela utiliser le bouton "FETCH" à coté avec la doucle flèche circulaire. Si il y a des modifications dans le projet, il faudra ensuite appuyer sur "PULL" en bas, après avoir fait "FETCH". Les modifications étant séléctionnées, vous pouvez alors faire "PULL" après avoir validé avec "COMMIT TO MASTER". Cela permettra de mettre la copie du texte en ligne sur GitBash. Faire de cette manière permet de donner les instructions à **GitBash** sans coder les commandes dans le terminal dans le cas où on a peur de mal coder les demandes. Les étapes en photos ci-dessous : 

![pull/push](../images/push.jpg)

Si on ne veut pas passer par  GitBash dans Atom directement, comme expliqué ci-dessus, on peut aussi passer par Gitbash en ouvrant le terminal et entrant les commandes "Git pull" et "git push". 

Attention !!  si on ne veut pas avoir de problème de **merge** (quand un même document subit plusieurs types de modifications et qu'il faut contrôler les modifications et choisir lesquels seront à prendre en compte ou à fusionner), il est préférable de toujours faire "Git pull" avant de modifier quoi que ce soit dans les fichiers. 


### Ajouter et modifier des photos

Pour les photos, il existe deux techniques. Soit on les ajoute via notre dossier référentiel, soit on les ajoute via **GitLab**. Dans le deuxième cas il faut aller dans ses documents, dans le dossier image, et aller sur "Upload files" comme le montre la photo ci-dessous : 

![ajouter image](../images/ajoutimage.JPG)

Ensuite pour ajouter votre texte à vos écrits, il va falloir entrer un code (voir image ci dessous) : 
- Le point d'exclamation signifie qu'on demande au site, via le code, d'afficher un caractère graphique (image,...) externe à la page se trouvant dans un autre dossier. 
- Le texte (non obligatoire) entre crochet correspond à la description rapide de l'image. Si le code est en erreur (mal écrit ou corrompu), c'est cela qui sera affiché. C'est bien de mettre un petit mot pour décrire l'image afin de s'y retrouver si il y a une erreur. 
- la parenthèse permet d'entrer le lieu et nom de stockage de la photographie, chaque espace est séparé par un slash. Dans l'exemple il y a deux points pour aller dans le sous dossier, puis un slash, puis "images" pour indiquer le dossier où se trouve l'image, puis slash, et enfin le nom de l'image uploadée au préalable. 

Attention, tu dois toujours mettre le nom exacte du fichier image et faire attention si c est ecrit .JPG ou .jpg ou .jpeg car si ce n est pas rédigé à l'identique par rapport au nom de l image, ca ne marche pas. Si tu oublies le point d'exclamation, tu auras juste un lien qui te mènera à ton image lorsque tu cliques dessus, mais cette dernière ne s'affichera pas directement dans ta documentation finale du module.

![](../images/codephoto.JPG)

Mais les images sont souvent trop grandes, ou trop volumineuses. Pour que la lecture de notre plateforme soit fluide, il est préférable de réduir les images, les modifier ou les compresser. Au début je passais plutôt par **Photoshop** malgré les recommandations, je connais bien ce logiciel et je sais être rapide pour modifier une image dessus. Mais j'ai voulu expérimenter **GraphicsMagick**, un logiciel recommandé pour faire cela, idéal parce que très rapide et très léger comme logiciel, il est compatible avec la quasi totalité des processeur et simple d'utilisation. J'ai donc téléchargé l'application. Il ne faut pas s'attendre à avoir une interface comme on aurait sur Gimp ou Photoshop, pour ce logiciel. Il fonctionne simplement avec  **Git BASH**, il faut demandé au terminal de faire les actions que je désire en lui indiquant avec quel logiciel le faire, et alors le terminal passera par le logiciel **GraphicsMagick** sans même l'ouvrir sur le PC et affichera un résultat imédiat ! Pour cela il faut coder la commande de la manière suivante :

![smallimage](../images/smallimage.JPG)

Petit conseil, ne prenez pas bêtement comme moi, le code sans remplacer le nouveau nom de la photo (comme le cas de la première commande). Sinon toutes vos photos s'appeleront "smallimage" !! 

![réduction](../images/réduction.JPG)

Au passage, pensez à ouvrir le termial dans le fichier où se trouve votre photo, sinon ça ne fonctionnera pas. Vous voilà pret pour ajouter du texte et des images. Pour ce qui est des caractères spéciaux liés au texte ils sont assez simples :

- L'ajout de ** autour du mot/phrase met la selection en gras 
- L'ajout de _ autour du mot/phrase met la selection en italique
- Si vous faite une citation, mettez> devant votre phrase pour qu'elle soit grisée et qu'il y est un décalage. 
- les points se font par la simple utilisation du tiret du 6 cumulé à un espace avant le texte qui suit. 
- Pour les site web ou autre lien, c'est entre parenthèse aussi, et on peut mettre la description entre crochet avant.


## QUELQUES LIENS UTILES

- [MKdocs](https://www.mkdocs.org/)
- [Git](https://git-scm.com/)
- [vidéo d'installation de Shell Bash](https://www.youtube.com/watch?v=CyG16N3GJWo&feature=emb_title)
- [Guide d'installation de Shell Bash](https://korben.info/installer-shell-bash-linux-windows-10.html)
- [GraphicsMagick](http://www.graphicsmagick.org/)
- [Atom et son package](https://atom.io/packages/markdown-preview-enhanced)
- [Fusion Git Bash et Atom](https://courses.cs.washington.edu/courses/cse154/19su/resources/assets/atomgit/windows/)


## RECAP' DE LA VULGARISATION

Voici plusieurs questions que je pense avoir compris, que certains m'ont posés, ça peut être utile pour comprendre l'intéret de ce module donc je les remet ici pour vulgariser un peu tout ça et rendre le module moins technique et plus accessible. J'ai déjà globalement expliqué ça plus haut, mais ça permet de faire un résumé condensé si à la fin de mes explications tu n'as pas compris pourquoi faire cela : 

> **Pourquoi passer par tout ça si on peut juste le faire sur GitLab ?** 
> 
> Parce que si on devient famillié avec cette technique, c'est beaucoup plus rapide à faire de cette manière.

> **Concrètement qu'est ce qui est quoi et à quoi ça sert ?**
> 
> En gros, il y a deux cas :
> - Il y a GitLab qui est la plateforme (une sorte de "blog") qui reprends tous les modules. C'est là où le résultat final apparaitra de tes explications d'apprentissage. Ces publications de modules seront publiques et permettront à tous de pouvoir apprendre à utiliser les machines à leur tour simplement en suivant tes conseils. En gros c'est la plateforme où tu vas pouvoir enseigner aux autres ce que tu as appris pour transmettre et partager tes connaissances à qui souhaite les recevoir, en le guidant par toutes les étapes par lesquels tu es passé. Tu peux directement travailler sur la page web Gitlab pour rédiger les modules et mettre les photos.
> - Pour faire les mêmes actions de manière plus rapide, avec un peu de maitrise, tu peux aussi utiliser deux choses : un logiciel de codage pour faire ça plus vite et un logiciel de texte. Donc en gros si tu suis les instructions fournies dans Fablab ULB > enseignements > 2020-2021 > FabZero > Basics > Repository (documentation), on va te demander d installer un logiciel Git Bash, qui est un terminal (un endroit où tu donnes des commandes à ton ordinateur sur quoi faire). tu dois aussi installer un logiciel de texte qui s appel Atom, où là, tu code ton texte comme sur gitlab, avec les symboles etc ... Et le but, c'est de travailler directement depuis ton ordinateur sans la page web de gitlab via ses deux logiciels. Sauf que pour ca il faut lier ton ordinateur à ton gitlab, et du coup faire plein de manipulations pour sécuriser la connexion (clefs ssh) et synchroniser ton gitlab à ton ordinateur. Quand c'est fait, tu as dans tes documents d'ordinateur sur un dossier identiques aux documents de ton Gitlab, avec le texte et les photos mis à jour, mais que tu peux modifier directement avec Atom sans passer par gitlab. En fait avec Atom tu ecris, et avec GitBash tu commandes : tu ordonnes à ton ordinateur de synchroniser le dossier de ton ordinateur à gitbash et il le fait. Donc concrètement, ça permet de travailler au "brouillon" sur ton ordinateur, conserver des copies de ton travail et mettre en ligne quand tu le souhaite le résultat grâce au terminal sans altérer ton travail précédant, ce qui est crucial si plusieurs personnes travaillent en même temps sur un même document.


## QUAND GITLAB EST FINI 

Quand on a terminé, que tout est encodé, que nos modules sont complétés et que nous n'avons plus rien à ajouter du point de vue du texte et des images, il reste quelques détails pour encore améliorer le rendu final de notre site web créé grâce à Gitlab (consultable [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/coordination/-/blob/master/README.md)) : 

- 1 : CONFIGURER LE SITE WEB

Pour cela il faut se rendre dans son fichier personnel MkDocs. Ce fichier est indispensable, il sert à configurer notre site web mais aussi à y modifier le rendu visuel (par exemple la couleur, le thème,...). Pour configurer et améliorer notre site il faudra donc se rendre dans le fichier mkdocs.yml (il se trouve à la base de l'endroit où repose notre projet). Ensuite, il est nécessaire d'éditer le contenu (comme pour un module) et remplacer les données encodées de référence par des données personnalisées. 
Comme le montre l'image ci-dessous, nous avions tous au commencement un document nommé Fiore Basile qui était notre fichier type de référence. Pour configurer le site web, il faut respecter les identifications personnalisées. Ainsi tous les documents intitulés "Fiore Basile" deviennent dans mon cas "Benyakhou Clémentine". Il faut au minimum configurer les 5 premières lignes (voir le avant après en jaune sur l'image). 

![configuration](../images/config.jpg)

On peut également jouer sur le thème, on en trouve des [exemples ici](https://mkdocs.github.io/mkdocs-bootswatch/?fbclid=IwAR2cojBDtkeUeoi47RgVidXlxqEEEgz7H4wbAiy_CHaIIV-mO5pMfJLkgG4), la typographie, le déploiement, ect du site. Comme le montre l'image ci-dessous, l'interface va varier si on change les paramètre. Ici, on a le cas d'un de mes camarades (Anthony) qui à laissé les paramètres de base et moi qui les ai modifié (voir modification entourée en bleu ci-dessus). J'ai ajouté le mot "Lux" en tant que nom de thème ce qui me permet d'avoir un thème noir et non bleu, de modifier ma typographie mais aussi avoir une interface plus "esthétique" et fluide selon moi. Les informations liées au guide d'utilisation pour agir sur les modifications se trouvent sur le [lien](https://www.mkdocs.org/user-guide/configuration/) suivant. 

![configuration 2](../images/clemantho.jpg)

- 2 : ACTIVER LES NOTIFICATIONS PAR E-MAIL 

La dernière étape consiste à activer les notifications de GitLab par e-mail. Pour recevoir ce genre de notification il suffit de changez son "niveau de notification global" en "regarder". Le manuel d'utilisation du site Gitlab précise d'ailleurs que si l'on travaille sur plusieurs projets en cours, il est préférable de spécifier à ce moment là sur quel "projet" on veut "regarder" les notifications.

Pour faire cela, il faut cliquer sur sa photo de profil, aller dans "reglages", puis regarder dans la colonne à gauche de l'écran l'onglet "notifications". Une fois sur la page des réglages de notification, on peut gérer les paramètres afin d'être informé par mail. A chaque fois que l'on modifie ces paramètres, ils sont automatiquement enregistrés et activés, il n'y a pas besoin de chercher un bouton pour les valider. 

![configuration 3](../images/notifi.jpg)

Comme le montre les trois photos ci-dessus, il faudra alors mettre "participer" à la place de "regarder" dans le niveau de notification global. Cela permettra de limiter les e-mails non désirables traitants des conversations auxquelles vous n'avez pas participé, mais tout de même recevoir les informations sur les activités où vous vous sentez concerné. Ensuite il faudra remplacé "global" par "regrder" sur les dits sujets où on souhaite être tenu informé par notification. Il existe d'autres possibilités que ces deux options, à vous de voir ce qui vous interesse en fonction de vos attentes. L'image à droite montre toutes les possibilités de type de notification. Il est d'ailleurs possible de mettre des adresses mail différentes en fonction des notifications et des projets. Cela n'est pas vraiment utile dans notre cas, mais essentiel si on travail à plusieurs sur des documents de groupe.

- 3 : VERIFIER LA CONFIGURATION

Le site web final est mis à jour automatiquement deux fois par semaine durant la nuit. On peut le consulter sur le lien fournit plus haut pour voir l'avancement du site après les mises à jour. Mais on peut également vérifier si tout s'est bien passé lors de celles-ci. Pour cela il faut se rendre  dans le menu de droite puis cliquer sur "CI/CD", puis cliquer sur "jobs". Si le fichier mkdocs (étape 1 de la configuration) est mal configuré, le travail s'affichera alors comme "failed" en rouge, dans le cas contraire, il sera vert. Pour ma part on peut constater qu'il y a eu des échecs mais que ma dernière action s'est bien passée on a donc les deux exemples concrets possibles : 

![configuration 4](../images/ci_cd.JPG)

Après avoir fait toutes les étapes, vous devriez avoir compris GitLab et commencez à bien le maitriser. Alors je vous invite à faire le tour de mes autres modules pour en apprendre davantage ! 

UPDATE !! Je vais plutot mettre le thème "Sandstone" car le thème "Lux" fais sauter le caractère **gras** de certains mots, ce qui est dommage. J'attends la mise-à-jour pour voir si ce thème est mieux.