## A PROPOS DE MOI

![](images/avatar-photo.jpg)

Salut ! Je suis Clémentine Benyakhou. J'étudie actuellement en première année de master en architecture. Cette année j'ai décidé d'explorer l'**option Design** à la Cambre-Horta (ULB), à Bruxelles. Je te propose de jeter un œil à mon site web pour mieux comprendre mon apprentissage dans cette option, j'y détaillerai progressivement les moindres informations sur mes découvertes et mon apprentissage ! Tu peux trouver des information sur mon site : [Clémentine Benyakhou](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/clementine.benyakhou/final-project/)

## MIEUX ME CONNAITRE

Pour me présenter rapidement, saches que je suis née à Chalon-sur-Saône, en Bourgogne (France). J'ai grandi 9 ans là-bas, puis j'ai déménagé dans les Cévennes. 
J'ai ensuite fais mes études supérieures à Montpellier, j'y ai réalisé une année de mise à niveau en art appliqué (**MANAA**), et ensuite eu mon diplôme en **Design d'Espace**. 
Il y a trois ans, une envie de découvrir de nouveaux horizons m'a mené à venir en Belgique. Je me suis aventurée dans les études d'**architecture** et me voilà là, en première année de master, en **option Design**.  

## COMPETENCES

Je possède une assez grande connaissance des logiciels informatiques, grâce à mes études je maîtrise la suite Adobe, la suite Office, des logiciels 2D et 3D (Sketchup, Archicad, Autocad, Fusion 360, ...) ainsi que des logiciels de rendus (Artlantis, Lumion, Twinmotion, ...). 

## EXEMPLE DE PROJET 

Je te présente un article qui a été écrit sur mon projet professionnel réalisé lors de ma dernière année de Design d'Espace. Mon travail à bien évolué depuis mais il reste une de mes premières grandes fiertés dans mon parcours. Tu peux le consulter en cliquant sur l'URL suivant : 

http://www.lechantdudesign.com/2018/07/16/montpellier-uchronie-micro-architecture-nomade-2018-de-ps01/?fbclid=IwAR1kKan-UnKJ3r0dmwcDqkW04B9LvA650MM2AFJU_mZKJF1_QloWTb_4Uo8

![image du projet](images/sample-pic-2.jpeg)

Ci-dessus, des images du projet.
Ci-dessous, la charte graphique de ce-dernier.

![charte graphique](images/sample-photo.jpg)

## CENTRE D'INTERET

Je n'ai pas de passion à proprement parler, mais j'ai plutôt une infinité d'intéret pour tout. J'aime beaucoup essayé et apprendre de nouvelles choses, je suis de nature touche à tout, très manuelle. Si j'ai choisi ce module, c'est parce qu'il s'agissait d'un défit de plus dans ma vie, une connaissance supplémentaire dans un domaine où j'ai, certes, des bases vu mon parcours, mais où je n'y connais finalement que peu de chose. 
Sinon j'ai plutôt la main verte et j'aime particulièrement créer des pots pour mes boutures récoltées au grés de mes voyages, balades et autres. Que ce soit en terre, en tissus où en bois, j'aime tester des nouvelles matières, leurs limites et modeler cela en fonction de mes émotions. Voici quelques unes de mes créations :

![](images/créa1.jpg)
![](images/créa2.jpg)

Pots, boucles d'oreille, cotons réutilisables, boutures, broderies, .... m'ont mené à créer ma marque (FRUKT) qui est un passe temps pour moi. Je les vends occasionnelement, ce n'est pas du tout un objectif marketing mais plus une activité que j'aime mener en parallèle de mes études. 


# EXERCICE DU FABLAB

> «_Qu’est ce que Poltronova si ce n’est une collection d’objets extrêmement visionnaires, en avance sur leur temps, agressivement anticonformistes,
> pleins de sensualité ? Ils sont nés loin du désir d’être créatifs, opposés à l’idée de concevoir quelque chose de pratique._
»
> 
> **_Roberta Meloni_**

![musée](images/musée.jpg)

### PRESENTATION

Lors de notre premier atelier d'option Design, nous nous sommes rendu au **Brussels Design Museum** à Heysel. Nous avons parcouru minutieusement ce musée dans le but d'y selectionner chacun un objet de l'exposition. Après ce temps de séléction, nous avons effectué un échange de groupe sur nos choix personnels et la raison de ce choix. Nous avons également fait un travail de recherche autour de ce dernier. L'objectif final avec cet objet sera de proposer une **réinterprétation**. 
Nous avons donc débuté une phase de 5 semaines de formation/initiation aux machines du FabLab (imprimante 3D, découpe Laser, ...). Phase après laquelle, il résultera de ces 5 semaines, une proposition de réinterprétation utilisant nos nouvelles compétences sur les machines du FabLab. 

## PROCESSUS

### CHOIX DE L'OBJET

J'ai décidé de travailler sur la lampe GHERPE de **Superstudio** en collaboration avec Poltronova. Réalisée en 1968, cette lampe est à la fois intriguante, modulable, intéractive et captivante. Ci-dessous, je t'ai fais une petite fiche de présentation pour que tu puisses faire sa connaissance. 

![planche de présentation](images/superstudio_gherpe.jpeg)

### RAISON DU CHOIX

Si j'ai choisi de travailler sur cette lampe c'est pour deux raisons : 

- D'abord, parce que j'ai eu un réel coup de coeur, pour sa forme, mais surtout pour sa modularité. Elle entretient un **rapport avec l'Homme** fort, elle vit par **interaction** avec ce dernier pour répondre à ses besoins. C'est en cela qu'elle est très différente d'une lampe standard, qui serait posée sur un meuble et aurait, comme seule zonne de contact/échange, un interrupeur. Ce qui m'a saisi, c'est sa **légèreté** dans la forme, l' **évidence** de sa foncion modulable : On comprend son fonctionnement simplement en la regardant, on sait que l'on peut la toucher, on à envie de l'essayer, de tester toutes les possibilité d'éclairage et de position, simplement en la regardant. Son design possède une sorte de clarté manifeste qui m'a conquise. 

- Ensuite, parce que lors de la visite du musée, j'ai fais 3 fois le tour des objets exposés, et malgré quelques objets remarquables, rien n'a retenu vraiment mon attention ... Jusqu'au moment où je l'ai vu. Elle m'est apparue lors de mon dernier tour. Timidement **posée sur une étagère**, effacée face aux objets qui l'entouraient, qui eux, étaient volumineux et imposants en comparaison. Elle n'était pas mise en avant du tout, tenue très éloignée du public par une estrade qui nous en **distancie**. Pourtant de tous les objets se trouvant sur cette partie de l'exposition, c'était celui dont le mécanisme était le plus fascinant. En plus de la difficulté d'être vue, il était difficile de trouver son cartouche. C'est certainement le paradoxe entre l'**ingéniosité mécanique** de la lampe et l'**insuffisance scénograpique** pour la valoriser qui m'a le plus donné envie d'en savoir plus sur elle. Telle un objet retrouvé dans un vieux grenier, elle a piqué ma curiosité.   

![exposition Design Museum Bruxelles](images/expo.jpg)



Maintenant que tu me connais un peu plus, que tu connais mes centres d'interet et mes choix de projet, découvre mon apprentissage des machines au FabLab en te rendant dans mes modules ! 